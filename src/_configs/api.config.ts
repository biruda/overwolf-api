'use strict';
import {IncomingHttpHeaders, IncomingMessage, ServerResponse} from 'http';
import {UrlWithStringQuery} from 'url';

export namespace IApiConfig {

  export namespace Options {

    export interface ReqModified extends IncomingMessage {
      headers: ReqHeaders;
      query: any;
      body: any;
      urlObj: UrlWithStringQuery;
    }

    export interface ResModified extends ServerResponse {
      start: number;
      url: string;
    }

    export interface ReqQuery {
    }

    export interface ReqHeaders extends IncomingHttpHeaders {
      authorization: string;
    }

    export type MethodField = 'get' | 'put' | 'post' | 'remove';

    export const $MethodList: MethodField[] = ['get', 'put', 'post', 'remove'];

    export interface MethodContent {
      get: any;
      put: any;
      post: any;
      remove: any;
    }
  }

  export namespace Data {

    export interface Routes extends IApiConfig.Options.MethodContent {
      get: RouteHandler;
      put: RouteHandler;
      post: RouteHandler;
      remove: RouteHandler;
    }

    export interface RoutesSource {
      [path: string]: Routes;
    }

    interface RouteHandler {
      [k: string]: (request: IApiConfig.Options.ReqModified, response: IApiConfig.Options.ResModified) => void;
    }
  }

  export interface Config {
    _host: string;
    _hostDev: string;
    url: {
      users: {
        authGoogle: string;
      };
      release: {
        note: string;
        notePublic: string;
      };
      apps: {
        app: string;
      };
    };
  }
}

const _apiConfig: IApiConfig.Config = {
  _host: 'https://api.overwolf.ai',
  _hostDev: 'http://127.0.0.1',
  url: {
    users: {
      authGoogle: 'user/auth/google',
    },
    release: {
      note: 'release/note',
      notePublic: 'release/notes',
    },
    apps: {
      app: 'apps/app',
    },
  },
};

export function getApiConfig(): IApiConfig.Config {
  return Object.assign({}, _apiConfig);
}


export function prepareRoutesGroup(source: IApiConfig.Data.RoutesSource): IApiConfig.Data.Routes {
  const _routes: IApiConfig.Data.Routes = {
    get: {},
    put: {},
    post: {},
    remove: {},
  };

  Object.keys(source).forEach((path: string) => {
    const mode: IApiConfig.Data.Routes = source[path];

    IApiConfig.Options.$MethodList.forEach((method: IApiConfig.Options.MethodField) => {
      Object.keys(mode[method]).forEach((key: string) => {
        // {[url: string]: Function}
        _routes[method][key] = mode[method][key];
      });
    });
  });

  return _routes;
}
