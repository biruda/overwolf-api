'use strict';

export namespace IDbConfig {

  export interface Config {
    _mongo: {
      host: string;
      hostDev: string;
      dbName: string;
      params: {
        auth: {
          user: string,
          password: string,
        },
        ssl: boolean,
        authSource: string,
        useNewUrlParser: boolean,
      };
    };
    tables: {
      users: {
        user: string,
      },
      apps: {
        app: string,
      },
      release: {
        note: string,
      },
    },
  }
}

const _dbConfig: IDbConfig.Config = {
  _mongo: {
    host: `mongodb://127.0.0.1`,
    hostDev: `mongodb://127.0.0.1`,
    dbName: 'overwolf',
    params: {
      auth: {
        user: 'vsouytcvhjcsc',
        password: 'scn87vdiuwe67yhsifwe7',
      },
      ssl: false,
      // authSource: 'admin'
      authSource: 'overwolf',
      useNewUrlParser: true
    },
  },
  tables: {
    users: {
      user: 'users_user',
    },
    apps: {
      app: 'apps_app',
    },
    release: {
      note: 'release_note',
    },
  },
};

export function getDbConfig(): IDbConfig.Config {
  return _dbConfig;
}
