export namespace IErrorConfig {

  export namespace Options {

    export type TypeField = 'critical' | 'error' | 'warning' | 'debug' | 'noCatch' | 'noConsole';

    export type NameField = 'Http' | 'Cron' | 'Server' | 'Api' | 'Notify' | 'Db' | 'Socket';
  }

  export interface Item {
    name: IErrorConfig.Options.NameField;
    type: IErrorConfig.Options.TypeField;
    message: string;
    params: object[];
    stack: string;
  }
}
