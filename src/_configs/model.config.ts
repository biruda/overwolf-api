'use strict';
// include other configs
import {getDbConfig, IDbConfig} from './db.config';


const dbConfigApi: IDbConfig.Config = getDbConfig();

export namespace IModelConfig {

  export namespace Options {

    export type DriverField = 'mongo' | 'file';
  }


  export interface Model {
    driverName: IModelConfig.Options.DriverField;
    table: string;
  }


  export interface Config {
    users: {
      user: IModelConfig.Model,
    };
    apps: {
      app: IModelConfig.Model,
    };
    release: {
      note: IModelConfig.Model,
    };
  }
}

const _modelConfig: IModelConfig.Config = {
  users: {
    user: {
      driverName: 'mongo',
      table: dbConfigApi.tables.users.user,
    },
  },
  apps: {
    app: {
      driverName: 'mongo',
      table: dbConfigApi.tables.apps.app,
    },
  },
  release: {
    note: {
      driverName: 'mongo',
      table: dbConfigApi.tables.release.note,
    },
  },
};

export function getModelConfig(): IModelConfig.Config {
  return Object.assign({}, _modelConfig);
}
