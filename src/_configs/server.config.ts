'use strict';

export namespace IServerConfig {

  export interface Config {
    api: {
      serverPortProd: number;
      serverPortDev: number;
      socketCode: string;
    };
    auth: {
      googleAuthProd: string,
      jwt: string,
      tokenExpiration: string, // jwt format
    };
  }
}

const _serverConfig: IServerConfig.Config = {
  api: {
    serverPortProd: 8081,
    serverPortDev: 80,
    socketCode: 'sdaBIUUDSNSDn23uTDSVHy^^%$#SAgas87asg54D%$fuj',
  },
  auth: {
    googleAuthProd: '197664436471-me5lnd5lbke8rdg66t182dktcck7gvmo.apps.googleusercontent.com',
    jwt: 'ynOHc78shH*(&^guih98HoBUIl',
    tokenExpiration: '7d', // jwt format
  }
};

export function getServerConfig(): IServerConfig.Config {
  return Object.assign({}, _serverConfig);
}