'use strict';
import {IErrorConfig} from '../_configs/error.config';

export class ErrorBase extends Error {
  public message: string;
  public info: string | number | object;

  constructor(message: string, name: IErrorConfig.Options.NameField, type: IErrorConfig.Options.TypeField, params: object[]) {
    super(message);
    this.info = params[0];

    if (type !== 'noCatch') {
      catchError({
        name: name,
        type: type,
        message: this.message,
        params: params,
        stack: this.stack
      });
    }
  }
}

export class HttpError extends ErrorBase {
  constructor(message: string, params: object[], type?: IErrorConfig.Options.TypeField) {
    super(message, 'Http', type !== void 0 ? type : 'debug', params);
  }
}

export class ServerError extends ErrorBase {
  constructor(message: string, params: object[], type?: IErrorConfig.Options.TypeField) {
    super(message, 'Server', type === void 0 ? 'critical' : type, params);
  }
}

export class ApiError extends ErrorBase {
  constructor(message: string, params: object[], type?: IErrorConfig.Options.TypeField) {
    super(message, 'Api', type === void 0 ? 'error' : type, params);
  }
}

export class DbError extends ErrorBase {
  constructor(message: string, params: object[], type?: IErrorConfig.Options.TypeField) {
    super(message, 'Db', type === void 0 ? 'error' : type, params);
  }
}

export class SocketError extends ErrorBase {
  constructor(message: string, params: object[], type?: IErrorConfig.Options.TypeField) {
    super(message, 'Socket', type === void 0 ? 'error' : type, params);
  }
}

function catchError(opts: IErrorConfig.Item) {
  const logData = `
  ${new Date().toISOString()}
  **********************************************************************************
  type:       ${opts.type};
  message:    ${opts.message};
  params:     ${JSON.stringify(opts.params)};
  stack:      ${opts.stack};
  **********************************************************************************` + '\n';

  if (opts.type === 'debug') {
    console.log('Error', 'log', logData);
  } else if (opts.type !== 'noConsole') {
    console.error('Error', 'error', logData);
  }
}
