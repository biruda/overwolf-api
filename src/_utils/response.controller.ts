'use strict';
// modules
import {ClientRequest, ClientRequestArgs, IncomingMessage} from 'http';
import {get, request} from 'https';
// controllers
import {ErrorBase} from './error.controller';
// config
import {IApiConfig} from '../_configs/api.config';


export namespace IResponseController {

  export interface Response {
    err: boolean;
    msg: string;
    data: object[];
  }
}

export namespace IRequestController {

  export namespace Send {

    export interface Opts {
      host: string;
      path: string;
      Authorization?: string;
    }

    export interface OptsData {
      host: string;
      path: string;
      body: {
        client_id: string;
        client_secret: string;
        refresh_token: string;
        grant_type: string;
      };
    }

    export type Cb = (err: null | Error, body: null | string) => void;
  }
}

export class ResponseController {

  static success(res: IApiConfig.Options.ResModified, content: object[]): void {
    const _data: IResponseController.Response = {err: false, msg: '', data: content};

    res.setHeader('Content-Type', 'application/json');
    res.writeHead(200);
    res.end(JSON.stringify(_data));

    ResponseControllerUtils.track('Success', res.url, Date.now() - res.start)
  };

  static fail(res: IApiConfig.Options.ResModified, err: ErrorBase): void {
    const _data: IResponseController.Response = {
      err: true,
      msg: err.message,
      data: [{desc: err.info}],
    };

    res.setHeader('Content-Type', 'application/json');
    res.writeHead(400);
    res.end(JSON.stringify(_data));

    ResponseControllerUtils.track('Fail', res.url, Date.now() - res.start)
  };

  static noUser(res: IApiConfig.Options.ResModified): void {
    const _data: IResponseController.Response = {
      err: true,
      msg: 'isRemoved',
      data: [{user: 'is removed'}]
    };

    res.setHeader('Content-Type', 'application/json');
    res.writeHead(403);
    res.end(JSON.stringify(_data));

    ResponseControllerUtils.track('Fail', res.url, Date.now() - res.start);
  };

  static noRoute(res: IApiConfig.Options.ResModified): void {
    const _data: IResponseController.Response = {
      err: true,
      msg: 'noExist',
      data: [{route: 'no exist'}]
    };

    res.setHeader('Content-Type', 'application/json');
    res.writeHead(404);
    res.end(JSON.stringify(_data));

    ResponseControllerUtils.track('Fail', res.url, Date.now() - res.start);
  };

  static restricted(res: IApiConfig.Options.ResModified): void {
    const _data: IResponseController.Response = {
      err: true,
      msg: 'noAuth',
      data: [{user: 'not authorised'}]
    };

    res.setHeader('Content-Type', 'application/json');
    res.writeHead(401);
    res.end(JSON.stringify(_data));

    ResponseControllerUtils.track('Fail', res.url, Date.now() - res.start);
  };
}

export class RequestController {

  static send(opts: IRequestController.Send.Opts, cb: IRequestController.Send.Cb): void {
    try {
      const _opts: ClientRequestArgs = {
        host: opts.host,
        path: opts.path,
      };
      if (opts.Authorization !== void 0) {
        _opts.headers = {
          Authorization: opts.Authorization,
        };
      }

      const req: ClientRequest = get(_opts, (res: IncomingMessage): void => {
        res.setEncoding('utf8');
        let body = '';
        res.on('data', (chunk: Buffer | string) => {
          body += chunk.toString();
        });
        res.on('error', (err: Error) => cb(err, null));
        res.on('end', () => cb(null, body));
      });
      req.on('error', (err: Error) => cb(err, null));
    } catch (e) {
      cb(e, null);
    }
  }

  static sendData(opts: IRequestController.Send.OptsData, cb: IRequestController.Send.Cb): void {
    try {
      const _opts: ClientRequestArgs = {
        method: 'POST',
        host: opts.host,
        path: opts.path,
      };

      const req: ClientRequest = request(_opts, (res: IncomingMessage): void => {
        res.setEncoding('utf8');
        let body = '';
        res.on('data', (chunk: Buffer | string) => {
          body += chunk.toString();
        });
        res.on('error', (err: Error) => cb(err, null));
        res.on('end', () => cb(null, body));
      });
      req.on('error', (err: Error) => cb(err, null));
      req.write(JSON.stringify(opts.body));
      req.end();
    } catch (e) {
      cb(e, null);
    }
  }
}

class ResponseControllerUtils {

  static track(status: string, route: string, duration: number): void {
    // TrackController.increment({driver: 'api'}, {
    //   api: {
    //     statusName: status,
    //     routeName: route,
    //     time: duration,
    //   }
    // });
  };
}
