'use strict';
// modules
import {parallel} from 'async';
import {ObjectID, UpdateWriteOpResult} from 'mongodb';
// controllers
import {IMongoController, MongoController} from './mongo.controller';
import {DbError} from '../../../_utils/error.controller';
// // models
// import {TrackDb} from '../../models/_system/tracker/TrackDb';
// configs
import {IModelConfig} from '../../../_configs/model.config';

export namespace IDBController {

  export namespace Options {

    export interface DriverConfig {
      driverName: IModelConfig.Options.DriverField;
      table: string;
    }
  }

  export namespace Params {

    export interface Params {
      mongo?: IMongoController.Params.Params;
    }

    export interface Search {
      mongo?: IMongoController.Params.Search;
    }

    export interface Update {
      mongo?: IMongoController.Params.Update;
    }

    export interface Aggregate {
      mongo?: IMongoController.Params.Aggregate;
    }

    export interface Data {
      mongo?: IMongoController.Params.Data;
    }
  }

  export namespace Actions {

    export type Cb = (err: null | DbError, data?: any[]) => void;

    export type CbEmpty = (err: null | DbError) => void;
  }
}

export class DBController {

  static connectAll(cb: { (err: null | DbError) }) {
    parallel([
      (callback: { (err: null | DbError) }) => MongoController.connect((err: null | DbError) => callback(err)),
    ], (err: null | DbError) => cb(err));
  }

  static disconnectAll(cb: { (err: null | string) }) {
    MongoController.close();

    cb(null);
  }

  // crud

  static set(configs: IDBController.Options.DriverConfig,
             data: IDBController.Params.Data,
             cb: IDBController.Actions.Cb) {
    const start = Date.now();
    if (configs.driverName === 'mongo') {
      return MongoController.insert(configs.table, data.mongo, (err: null | DbError, data: ObjectID[]) => {
        cb(err, data);
        DBControllerUtils.track('mongo', 'insert', configs.table, data, err, start);
      });
    } else {
      cb(new DbError('wrong driver', [{driver: configs.driverName}]));
    }
  };

  static setOrUpdate(configs: IDBController.Options.DriverConfig,
                     search: IDBController.Params.Search,
                     data: IDBController.Params.Data,
                     cb: IDBController.Actions.Cb) {
    const start = Date.now();
    if (configs.driverName === 'mongo') {
      return MongoController.findOneReplace(configs.table, search.mongo, data.mongo, (err: null | DbError, data) => {
        cb(err, data);
        DBControllerUtils.track('mongo', 'createOrUpdate', configs.table, data, err, start);
      });
    } else {
      cb(new DbError('wrong driver', [{driver: configs.driverName}]));
    }
  };

  static get(configs: IDBController.Options.DriverConfig,
             params: IDBController.Params.Params,
             cb: IDBController.Actions.Cb) {
    const start = Date.now();
    if (configs.driverName === 'mongo') {
      return MongoController.find(configs.table, params.mongo, (err: null | DbError, data) => {
        cb(err, data);
        DBControllerUtils.track('mongo', 'read', configs.table, data, err, start);
      });
    } else {
      cb(new DbError('wrong driver', [{driver: configs.driverName}]));
    }
  };

  static distinct(configs: IDBController.Options.DriverConfig,
                  params: IDBController.Params.Params,
                  cb: IDBController.Actions.Cb) {
    const start = Date.now();
    if (configs.driverName === 'mongo') {
      return MongoController.distinct(configs.table, params.mongo, (err: null | DbError, data) => {
        cb(err, data);
        DBControllerUtils.track('mongo', 'distinct', configs.table, data, err, start);
      });
    } else {
      cb(new DbError('wrong driver', [{driver: configs.driverName}]));
    }
  };

  static aggregate(configs: IDBController.Options.DriverConfig,
                   aggregate: IDBController.Params.Aggregate,
                   cb: IDBController.Actions.Cb) {
    const start = Date.now();
    if (configs.driverName === 'mongo') {
      return MongoController.aggregate(configs.table, aggregate.mongo, (err: null | DbError, data) => {
        cb(err, data);
        DBControllerUtils.track('mongo', 'aggregate', configs.table, data, err, start);
      });
    } else {
      cb(new DbError('wrong driver', [{driver: configs.driverName}]));
    }
  };

  static update(configs: IDBController.Options.DriverConfig,
                search: IDBController.Params.Search,
                update: IDBController.Params.Update,
                cb: IDBController.Actions.Cb) {
    const start = Date.now();
    if (configs.driverName === 'mongo') {
      if (search.mongo === void 0) {
        return cb(new DbError('wrong driver', [{driver: configs.driverName, configs, search}]));
      }
      if (update.mongo === void 0) {
        return cb(new DbError('wrong driver', [{driver: configs.driverName, configs, update}]));
      }

      MongoController.update(configs.table, search.mongo, update.mongo, (err: null | DbError, data: [UpdateWriteOpResult]) => {
        cb(err, data);
        DBControllerUtils.track('mongo', 'update', configs.table, data, err, start);
      });
    } else {
      cb(new DbError('wrong driver', [{driver: configs.driverName}]));
    }
  };

  static count(configs: IDBController.Options.DriverConfig,
               params: IDBController.Params.Params,
               cb: IDBController.Actions.Cb) {
    const start = Date.now();
    if (configs.driverName === 'mongo') {
      return MongoController.count(configs.table, params.mongo, (err: null | DbError, data: [number]) => {
        cb(err, data);
        DBControllerUtils.track('mongo', 'count', configs.table, data, err, start);
      });
    } else {
      cb(new DbError('wrong driver', [{driver: configs.driverName}]));
    }
  };

  static remove(configs: IDBController.Options.DriverConfig,
                search: IDBController.Params.Search,
                cb: IDBController.Actions.CbEmpty) {
    const start = Date.now();
    if (configs.driverName === 'mongo') {
      if (search.mongo === void 0) {
        return cb(new DbError('wrong driver', [{driver: configs.driverName, configs, search}]));
      }

      MongoController.remove(configs.table, search.mongo, (err: null | DbError) => {
        cb(err);

        DBControllerUtils.track('mongo', 'remove', configs.table, [], err, start);
      });
    } else {
      cb(new DbError('wrong driver', [{driver: configs.driverName}]));
    }
  };
}

class DBControllerUtils {

  static track(driverName: string, methodName: string, tableName: string, data: any[] | [UpdateWriteOpResult], err: any, start: number): void {
    // // analytics (internal to model)
    // let count = 0;
    // try {
    //   count = data.length;
    // } catch (e) {
    // }
    // TrackDb.increment({
    //   driverName: driverName,
    //   methodName: methodName,
    //   tableName: tableName,
    //   time: Date.now() - start,
    //   isError: err !== null,
    //   dataLength: count
    // });
  }
}
