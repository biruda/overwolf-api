'use strict';
// modules
import {Db, InsertOneWriteOpResult, InsertWriteOpResult, MongoClient, ObjectID, UpdateWriteOpResult} from 'mongodb';
// controllers
import {DbError} from '../../../_utils/error.controller';
// configs
import {getDbConfig, IDbConfig} from '../../../_configs/db.config';


export namespace IMongoController {

  export namespace Options {

    export interface MongoUpdate {
      $set?: { [k: string]: any };
      $unset?: { [k: string]: any };
      $inc?: { [k: string]: any };
      $push?: { [k: string]: any };
      $min?: { [k: string]: any };
      $max?: { [k: string]: any };
    }

    export interface MongoFilter {
      // fields?: string[] | { [k: string]: number };
      projection?: { [k: string]: number };
      field?: string;
      skip?: number;
      limit?: number;
      sort?: { [k: string]: -1 | 1 };
    }
  }

  export namespace Params {

    export interface Search {
      _id?: ObjectID | SearchItem;
      email?: string | SearchItem;
      AppID?: ObjectID | SearchItem;
      VersionNumber?: number | SearchItem;
      Published?: boolean | SearchItem;
      $or?: Search[];
    }

    interface SearchItem {
      $gt?: string | number | Date;
      $gte?: string | number | Date;
      $lt?: string | number | Date;
      $lte?: string | number | Date;
      $in?: Array<string | null | boolean | ObjectID>;
      $nin?: Array<string | null | boolean>;
      $ne?: string | object | boolean;
      $exists?: boolean;
      $regex?: RegExp;
    }

    export interface Update {
      $set?: { [k: string]: any };
      $unset?: { [k: string]: any };
      $inc?: Increment;
      $max?: Increment;
      $min?: Increment;
      $push?: { [k: string]: any };
      upsert?: boolean;
    }

    interface Increment {
      points?: number;
      count?: number;
      'dealsHistory.stock'?: number;
      'dealsHistory.sold'?: number;
    }

    export type Data = { [k: string]: any } | { [k: string]: any } [];

    export interface Params {
      search?: Search;
      fields?: Fields;
      field?: string;
      limit?: number;
      skip?: number;
      sort?: { [k: string]: -1 | 1 };
    }

    export interface Fields {
      [k: string]: 0 | 1;
    }

    export interface Aggregate {
      $match?: Search;
      $geoNear?: {
        near: {
          type: string,
          coordinates: number[],
        },
        distanceField: string;
        maxDistance?: number;
        includeLocs?: string;
        num: number;
        spherical: boolean;
      };
      $sort?: AggregateSort;
      $group?: AggregateGroup;
      $groupList?: AggregateGroup[];
      $lookup?: AggregateLookup;
      $lookupList?: AggregateLookup[];
      $unwind?: string;
      $limit?: number;
      $skip?: number;
      $project?: AggregateProject;
    }

    export interface AggregateSort {
      [k: string]: -1 | 1;
    }

    export interface AggregateLookup {
      from: string;
      localField: string;
      foreignField: string;
      as: string;
    }

    export interface AggregateGroup {
      [k: string]: any;
    }

    export interface AggregateProject {
      [k: string]: AggregateProjectValue;
    }

    export type AggregateProjectValue = number | { $slice: (string | number)[] } | { $setUnion: string[] };
  }

  export namespace Storage {

    export interface Internal {
      connection: Db;
      configs: Configs;
    }

    interface Configs {
      _dbConfig: IDbConfig.Config;
    }
  }

  export namespace Actions {

    export type Cb = (err: null | DbError, data?: any[]) => void;

    export type CbInsert = (err: null | DbError, data?: ObjectID[]) => void;

    export type CbEmpty = (err: null | DbError) => void;
  }
}

const _Internal: IMongoController.Storage.Internal = {
  connection: null,
  configs: {
    _dbConfig: getDbConfig(),
  },
};

export class MongoController {

  static connect(cb: IMongoController.Actions.CbEmpty) {
    // connect only once
    if (_Internal.connection !== null) {
      return;
    }

    // connect to prod | dev database
    let host;
    if (process.env['NODE_ENV'] === 'development') {
      console.log('MongoController - APP', 'connect to dev');
      host = _Internal.configs._dbConfig._mongo.hostDev;
    } else {
      console.log('MongoController - APP', 'connect to prod');
      host = _Internal.configs._dbConfig._mongo.host;
    }

    // init connection
    MongoClient.connect(host, _Internal.configs._dbConfig._mongo.params)
      .then((connect: MongoClient) => {
        _Internal.connection = connect.db(_Internal.configs._dbConfig._mongo.dbName);
        cb(null)
      })
      .catch((e) => cb(new DbError('mongo error', [{message: e.message, name: e.name}])));
  }

  static close() {
    _Internal.connection['close']();
    _Internal.connection = null;
  };

  static insert(name: string,
                data: IMongoController.Params.Data,
                cb: IMongoController.Actions.CbInsert): void {
    if (Array.isArray(data)) {
      _Internal.connection.collection(name).insertMany(data)
        .then((d: InsertWriteOpResult) => cb(null, (<any>Object).values(d.insertedIds)))
        .catch((e) => cb(new DbError('mongo error', [{message: e.message, collection: name, data: data}])));
    } else {
      _Internal.connection.collection(name).insertOne(data)
        .then((d: InsertOneWriteOpResult) => cb(null, [d.insertedId]))
        .catch((e) => cb(new DbError('mongo error', [{message: e.message, collection: name, data: data}])));
    }
  }

  static aggregate(name: string,
                   aggregate: IMongoController.Params.Aggregate,
                   cb: IMongoController.Actions.Cb): void {
    const pipeline = [];
    if (aggregate.$geoNear !== void 0) {
      pipeline.push({$geoNear: aggregate.$geoNear});
    }
    if (aggregate.$lookup !== void 0) {
      pipeline.push({$lookup: aggregate.$lookup});
    }
    if (aggregate.$lookupList !== void 0) {
      for (let i = 0, len = aggregate.$lookupList.length; i < len; i++) {
        pipeline.push({$lookup: aggregate.$lookupList[i]});
      }
    }
    if (aggregate.$unwind !== void 0) {
      pipeline.push({$unwind: aggregate.$unwind});
    }
    if (aggregate.$match !== void 0) {
      pipeline.push({$match: aggregate.$match});
    }
    if (aggregate.$sort !== void 0) {
      pipeline.push({$sort: aggregate.$sort});
    }
    if (aggregate.$group !== void 0) {
      pipeline.push({$group: aggregate.$group});
    }
    if (aggregate.$groupList !== void 0) {
      for (let i = 0, len = aggregate.$groupList.length; i < len; i++) {
        pipeline.push({$group: aggregate.$groupList[i]});
      }
    }
    if (aggregate.$skip !== void 0) {
      pipeline.push({$skip: aggregate.$skip});
    }
    if (aggregate.$limit !== void 0) {
      pipeline.push({$limit: aggregate.$limit});
    }
    if (aggregate.$project !== void 0) {
      pipeline.push({$project: aggregate.$project});
    }

    _Internal.connection.collection(name).aggregate(pipeline).toArray()
      .then((d) => {
        cb(null, d);
      })
      .catch((e) => cb(new DbError('mongo error', [{message: e.message, collection: name, data: aggregate}])));
  }

  static find(name: string,
              params: IMongoController.Params.Params,
              cb: IMongoController.Actions.Cb): void {
    const _search = params.search || {};
    _Internal.connection.collection(name)
      .find(params.search !== void 0 ? params.search : {})
      .skip(params.skip !== void 0 ? params.skip : 0)
      .limit(params.limit !== void 0 ? params.limit : 0)
      .project(params.fields !== void 0 ? params.fields : {})
      .sort(params.sort !== void 0 ? params.sort : {})
      .toArray()
      .then((d) => cb(null, d))
      .catch((e) => cb(new DbError('mongo error', [{message: e.message, collection: name, data: params}])));
  }

  static findOne(name: string,
                 params: IMongoController.Params.Params,
                 cb: IMongoController.Actions.Cb): void {
    const _search = params.search || {};
    const _filter: IMongoController.Options.MongoFilter = {};
    if (params.fields !== null) {
      // _filter.fields = params.fields; // fields from set to return
      _filter.projection = params.fields; // fields from set to return
    }
    if (params.limit !== null) {
      _filter.limit = params.limit;
    }
    if (params.sort !== null) {
      _filter.sort = params.sort;
    }

    _Internal.connection.collection(name).findOne(_search, _filter)
      .then((d) => cb(null, d))
      .catch((e) => cb(new DbError('mongo error', [{message: e.message, collection: name, data: params}])));
  }

  static findOneReplace(name: string,
                        search: IMongoController.Params.Search,
                        data: object,
                        cb: IMongoController.Actions.Cb): void {
    _Internal.connection.collection(name).findOneAndReplace(search, data, {
      upsert: true
    })
      .then((d) => cb(null, [d]))
      .catch((e) => cb(new DbError('mongo error', [{
        message: e.message,
        collection: name,
        search: search,
        data: data
      }])));
  }

  static distinct(name: string,
                  params: IMongoController.Params.Params,
                  cb: IMongoController.Actions.Cb): void {
    const _search = params.search || {};
    const _filter: IMongoController.Options.MongoFilter = {};
    if (params.fields !== void 0) {
      // _filter.fields = params.fields; // fields from set to return
      _filter.projection = params.fields; // fields from set to return
    }
    if (params.limit !== void 0) {
      _filter.limit = params.limit;
    }
    if (params.sort !== void 0) {
      _filter.sort = params.sort;
    }

    _Internal.connection.collection(name).distinct(params.field, _search)
      .then((d) => {
        cb(null, d);
      })
      .catch((e) => cb(new DbError('mongo error', [{message: e.message, collection: name, data: params}])));
  }

  static count(name: string,
               params: IMongoController.Params.Params,
               cb: IMongoController.Actions.Cb): void {
    const _search = params.search !== void 0 ? params.search : {};

    _Internal.connection.collection(name).count(_search)
      .then((d) => cb(null, [d]))
      .catch((e) => cb(new DbError('mongo error', [{message: e.message, collection: name, data: params}])));
  }

  static update(name: string,
                search: IMongoController.Params.Search,
                update: IMongoController.Params.Update,
                cb: IMongoController.Actions.Cb): void {
    const _update: IMongoController.Options.MongoUpdate = {};
    if (update.$set !== void 0) {
      _update.$set = update.$set;
    }
    if (update.$unset !== void 0) {
      _update.$unset = update.$unset;
    }
    if (update.$inc !== void 0) {
      _update.$inc = update.$inc;
    }
    if (update.$max !== void 0) {
      _update.$max = update.$max;
    }
    if (update.$min !== void 0) {
      _update.$min = update.$min;
    }
    if (update.$push !== void 0) {
      _update.$push = update.$push;
    }

    const _options: { upsert?: true } = {};
    if (update.upsert === true) {
      _options.upsert = true;
    }

    _Internal.connection.collection(name).updateMany(search, _update, _options)
      .then((d: UpdateWriteOpResult) => cb(null, [d]))
      .catch((e) => cb(new DbError('mongo error', [{
        message: e.message,
        collection: name,
        data: search,
        update: update
      }])));
  }

  static remove(name: string,
                search: IMongoController.Params.Search,
                cb: IMongoController.Actions.Cb): void {
    _Internal.connection.collection(name)
      .deleteMany(search, {})
      .then((d) => cb(null, [d]))
      .catch((e) => cb(new DbError('mongo error', [{message: e.message, collection: name, data: search}])));
  }
}
