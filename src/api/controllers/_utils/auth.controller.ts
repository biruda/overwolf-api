'use strict';
// modules
import {OAuth2Client} from 'google-auth-library';
import {LoginTicket, TokenPayload} from 'google-auth-library/build/src/auth/loginticket';
// services
// controllers
import {CryptoController, ICryptoController} from './crypto.controller';
// models
// configs
import {getServerConfig, IServerConfig} from '../../../_configs/server.config';
import {IApiConfig} from "../../../_configs/api.config";

export namespace IAuthController {

  export namespace Data {

    export interface GoogleProfile {
      email: string;
      picture: null | string;
      name: null | string;
    }

    export type DecodedProfile = ICryptoController.Encrypt.EncryptUser;
  }

  export namespace Storage {

    export interface Internal {
      exemplars: Exemplars;
      configs: Configs;
    }

    interface Exemplars {
      googleAuthDev: any;
      googleAuthProd: any;
    }

    interface Configs {
      _serverConfig: IServerConfig.Config;
    }
  }

  export namespace Create {

    export interface Opts {
      id: string;
    }
  }
}

const _Internal: IAuthController.Storage.Internal = {
  configs: {
    _serverConfig: getServerConfig(),
  },
  exemplars: {
    googleAuthDev: null,
    googleAuthProd: null,
  }
};
_Internal.exemplars.googleAuthProd = new OAuth2Client({
  clientId: _Internal.configs._serverConfig.auth.googleAuthProd
});

export class AuthController {

  static createToken(opts: IAuthController.Create.Opts): string | null {
    const _Profile: ICryptoController.Encrypt.Data = {
      user: {
        id: opts.id,
      },
    };

    return CryptoController.encrypt('jwt', _Profile);
  }

  static checkSecure(req: IApiConfig.Options.ReqModified): IAuthController.Data.DecodedProfile | null {
    if (req.headers === void 0 || req.headers.authorization === void 0) {
      return null;
    }

    const authorization: string | string[] = req.headers.authorization;
    const header: string = typeof authorization === 'string' ? authorization : authorization[0];
    const token = header.replace('Bearer ', '');
    const decoded: ICryptoController.Decrypt.Data | null = CryptoController.decrypt('jwt', token.trim());
    return decoded !== null && decoded.user !== void 0 ? decoded.user : null;
  }

  static checkToken(token: string = ''): IAuthController.Data.DecodedProfile | null {
    const decoded: ICryptoController.Decrypt.Data | null = CryptoController.decrypt('jwt', token.trim());

    return decoded === null && decoded.user !== void 0 ? decoded.user : null;
  }

  static async checkSecureOauthGoogle(token: string): Promise<null | IAuthController.Data.GoogleProfile> {
    const ticketProd: null | LoginTicket = await _Internal.exemplars.googleAuthProd.verifyIdToken({
      idToken: token,
      audience: _Internal.configs._serverConfig.auth.googleAuthProd
    }).catch((e) => null);

    if (ticketProd === null) {
      return null;
    }

    const profile: TokenPayload = ticketProd.getPayload();
    if (profile.email_verified === true) {
      return {
        email: profile.email,
        picture: profile.picture !== void 0 ? profile.picture : null,
        name: profile.name !== void 0 ? profile.name : null,
      };
    } else {
      return null;
    }
  }
}
