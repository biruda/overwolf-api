'use strict';
// modules
import {sign, SignOptions, verify} from 'jsonwebtoken';
import {createHash} from 'crypto';
// services
import {ServerError} from '../../../_utils/error.controller';
// configs
import {getServerConfig, IServerConfig} from '../../../_configs/server.config';


export namespace ICryptoController {

  export namespace Options {

    export type Drivers = 'jwt';
  }

  export namespace Storage {

    export interface Internal {
      configs: Configs;
    }

    interface Configs {
      serverConfig: IServerConfig.Config;
    }
  }

  export namespace Encrypt {

    export interface Opts {
      expiresIn?: string;
      isNonExpired?: boolean;
    }

    export interface Data {
      user?: ICryptoController.Encrypt.EncryptUser;
    }

    export interface EncryptUser {
      id: string;
    }
  }

  export namespace Decrypt {

    export interface Data extends Object, ICryptoController.Encrypt.Data {
      iat?: number;
      exp?: number;
    }
  }
}

const _Internal: ICryptoController.Storage.Internal = {
  configs: {
    serverConfig: getServerConfig(),
  }
};

export class CryptoController {

  static encrypt(driver: ICryptoController.Options.Drivers,
                 data: ICryptoController.Encrypt.Data,
                 opts?: ICryptoController.Encrypt.Opts): null | string {
    if (driver === 'jwt') {
      const _SignOptions: SignOptions = {};
      if (opts.isNonExpired !== true) {
        _SignOptions.expiresIn = opts.expiresIn !== void 0
          ? opts.expiresIn
          : _Internal.configs.serverConfig.auth.tokenExpiration;
      }

      return sign(data, _Internal.configs.serverConfig.auth.jwt, _SignOptions);
    } else {
      new ServerError('wrong driver', [driver]);
      return null;
    }
  }

  static decrypt(driver: ICryptoController.Options.Drivers, data: string): ICryptoController.Decrypt.Data | null {
    if (driver === 'jwt') {
      try {
        const decodedObject: object | string = verify(data, _Internal.configs.serverConfig.auth.jwt);
        return decodedObject === void 0 || typeof decodedObject === 'string' ? null : decodedObject;
      } catch (e) {
        return null;
      }
    } else {
      new ServerError('wrong driver', [driver]);
      return null;
    }
  }

  static createHash(data: string): string {
    const hash = createHash('sha256');
    hash.update(data + Date.now());

    return hash.digest('hex');
  }
}
