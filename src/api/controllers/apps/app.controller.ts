'use strict';
// modules
import {waterfall} from 'async';
import {ObjectID} from 'mongodb';
// controllers
import {ResponseController} from '../../../_utils/response.controller';
import {DbError} from '../../../_utils/error.controller';
import {AuthController, IAuthController} from '../_utils/auth.controller';
// models
import {App, IApp} from '../../models/apps/app';
import {IUser, User} from '../../models/users/user';
// configs
import {IApiConfig} from '../../../_configs/api.config';

export namespace IAppController {

  export namespace Get {

    export interface Query {
      AppID?: string;
    }

    export type Response = IApp.Data.Item[];
  }

  export namespace Create {

    export interface Body {
      AppName: string;
    }

    export type Response = [{ appID: string }];
  }
}

export class AppController {

  static get(req: IApiConfig.Options.ReqModified, res: IApiConfig.Options.ResModified): void {
    const _Query: IAppController.Get.Query = req.query;

    // authentication part
    const profile: IAuthController.Data.DecodedProfile | null = AuthController.checkSecure(req);
    if (profile === null) {
      return ResponseController.restricted(res);
    }

    const _Data: { apps: IApp.Data.Item[] } = {apps: []};
    waterfall([

      (callback: { (err: null | DbError) }) => {
        if (_Query.AppID !== void 0) {
          return callback(null);
        }

        App.getAll((err: null | DbError, apps?: IApp.Data.Item[]) => {
          if (apps !== void 0) {
            _Data.apps = apps;
          }

          return callback(err);
        });
      },

      (callback: { (err: null | DbError) }) => {
        if (_Query.AppID === void 0) {
          return callback(null);
        }

        const _opts: IApp.Get.Opts = {
          id: _Query.AppID,
        };
        App.get(_opts, (err: null | DbError, apps?: IApp.Data.Item[]) => {
          if (apps !== void 0) {
            _Data.apps = apps;
          }

          return callback(err);
        });
      },

    ], (err: null | DbError) => {
      if (err !== null) {
        return ResponseController.fail(res, err);
      }

      ResponseController.success(res, _Data.apps);
    });
  }

  static set(req: IApiConfig.Options.ReqModified, res: IApiConfig.Options.ResModified): void {
    const _Body: IAppController.Create.Body = req.body;

    // authentication part
    const profile: IAuthController.Data.DecodedProfile | null = AuthController.checkSecure(req);

    console.log({profile})

    if (profile === null) {
      return ResponseController.restricted(res);
    }

    waterfall([

      // check user' rights | just user exists for now
      (callback: { (err: null | true | DbError, userID?: ObjectID) }) => {
        const _opts: IUser.Get.Opts = {
          id: profile.id,
        };
        User.get(_opts, (err: null | DbError, users?: IUser.Data.Item[]) => {
          if (err !== null) {
            return callback(err);
          }

          if (users === void 0 || users[0] === void 0) {
            callback(true);
          } else {
            callback(null, users[0]._id);
          }
        });
      },

      (userID: ObjectID, callback: { (err: null | DbError, appID: string) }) => {
        const _opts: IApp.Create.Opts = {
          AppName: _Body.AppName,
          Author: userID,
        };
        App.create(_opts, (err: null | DbError, appID?: string) => {

          callback(err, appID !== void 0 ? appID : null);

        });
      },

    ], (err: null | true | DbError, appID: string) => {
      if (err === true) {
        return ResponseController.restricted(res);
      }

      if (err !== null) {
        return ResponseController.fail(res, err);
      }

      const dataResponse: IAppController.Create.Response = [{appID}];
      ResponseController.success(res, dataResponse);
    });
  }
}
