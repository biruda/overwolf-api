'use strict';
// modules
import {waterfall} from 'async';
import {ObjectID} from 'mongodb';
// controllers
import {AuthController, IAuthController} from '../_utils/auth.controller';
import {ApiError, DbError} from '../../../_utils/error.controller';
import {ResponseController} from '../../../_utils/response.controller';
// models
import {INote, Note} from '../../models/release/note';
import {App, IApp} from '../../models/apps/app';
// configs
import {IApiConfig} from '../../../_configs/api.config';


export namespace INoteController {

  export namespace Get {

    export interface Query {
      AppID: string;
    }

    export type Response = INote.Data.Item[];
  }

  export namespace GetPublic {

    export interface Query {
      AppID: string;
      VersionNumber: number;
      onPage: number
      pageInd: number;

    }

    export type Response = [{
      itemsOnPage: number;
      pageNumber: number;
      notes: INote.Data.ItemPublic[];
    }];
  }

  export namespace Create {

    export interface Body {
      AppID: string;
      VersionNumber: number;
      ReleaseDate: Date;
      ReleaseNotes: string;
      Published: boolean;
    }

    export type Response = undefined[];
  }

  export namespace Update {

    export interface Body {
      noteID: string;
      ReleaseNotes?: string;
      Published?: boolean;
    }

    export type Response = undefined[];
  }
}

export class NoteController {

  static getPublic(req: IApiConfig.Options.ReqModified, res: IApiConfig.Options.ResModified): void {
    const _Query: INoteController.GetPublic.Query = req.query;

    const opts: INote.Get.OptsPublic = {
      AppID: _Query.AppID,
      VersionNumber: _Query.VersionNumber,
      offset: _Query.pageInd * _Query.onPage,
      limit: _Query.onPage,
    };
    Note.getPublic(opts, (err: null | DbError, notes?: INote.Data.Item[]) => {
      if (err !== null) {
        return ResponseController.fail(res, err);
      }

      const dataResponse: INoteController.GetPublic.Response = [{
        itemsOnPage: _Query.onPage,
        pageNumber: _Query.pageInd,
        notes
      }];
      ResponseController.success(res, dataResponse);
    });
  }

  static get(req: IApiConfig.Options.ReqModified, res: IApiConfig.Options.ResModified): void {
    const _Query: INoteController.Get.Query = req.query;

    // authentication part
    const profile: IAuthController.Data.DecodedProfile | null = AuthController.checkSecure(req);
    if (profile === null) {
      return ResponseController.restricted(res);
    }

    const opts: INote.Get.Opts = {
      AppID: _Query.AppID,
    };
    Note.get(opts, (err: null | DbError, notes?: INote.Data.Item[]) => {
      if (err !== null) {
        return ResponseController.fail(res, err);
      }

      ResponseController.success(res, notes);
    });
  }

  static set(req: IApiConfig.Options.ReqModified, res: IApiConfig.Options.ResModified): void {
    const _Body: INoteController.Create.Body = req.body;

    // authentication part
    const profile: IAuthController.Data.DecodedProfile | null = AuthController.checkSecure(req);
    if (profile === null) {
      return ResponseController.restricted(res);
    }

    waterfall([

      // validate app ID
      (callback: { (err: null | DbError, appID?: ObjectID) }) => {
        const _opts: IApp.Get.Opts = {
          id: _Body.AppID,
        };
        App.get(_opts, (err: null | DbError, app?: IApp.Data.Item[]) => {
          if (err !== void 0) {
            return callback(err);
          }

          if (app === void 0 || app[0] !== void 0) {
            return callback(new ApiError('bad params', [{msg: 'no valid app'}]));
          }

          callback(null, app[0]._id);
        });
      },

      // create
      (appID: ObjectID, callback: { (err: null | DbError) }) => {
        const _opts: INote.Create.Opts = {
          AppID: appID,
          VersionNumber: _Body.VersionNumber,
          ReleaseDate: _Body.ReleaseDate,
          ReleaseNotes: _Body.ReleaseNotes,
          Published: _Body.Published,
        };
        Note.create(_opts, (err: null | DbError) => callback(err));
      },

    ], (err: null | DbError) => {
      if (err !== null) {
        return ResponseController.fail(res, err);
      }

      const _Response: INoteController.Create.Response = [];
      ResponseController.success(res, _Response);
    });
  }

  static update(req: IApiConfig.Options.ReqModified, res: IApiConfig.Options.ResModified): void {
    const _Body: INoteController.Update.Body = req.body;

    // authentication part
    const profile: IAuthController.Data.DecodedProfile | null = AuthController.checkSecure(req);
    if (profile === null) {
      return ResponseController.restricted(res);
    }

    if (_Body.noteID === void 0) {
      return ResponseController.fail(res, new ApiError('bad params', [{msg: 'wrong note'}]));
    }
    if (_Body.Published === void 0 && _Body.ReleaseNotes === void 0) {
      return ResponseController.fail(res, new ApiError('bad params', [{msg: 'wrong info'}]));
    }

    const _opts: INote.Update.Opts = {
      noteID: _Body.noteID,
    };
    if (_Body.Published !== void 0) {
      _opts.Published = _Body.Published;
    }
    if (_Body.ReleaseNotes !== void 0) {
      _opts.ReleaseNotes = _Body.ReleaseNotes;
    }
    Note.update(_opts, (err: null | DbError) => {
      if (err !== null) {
        return ResponseController.fail(res, err);
      }

      const _Response: INoteController.Update.Response = [];
      ResponseController.success(res, _Response);
    });
  }
}
