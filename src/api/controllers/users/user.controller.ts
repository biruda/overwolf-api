'use strict';
// modules
import {waterfall} from 'async';
// controllers
import {ResponseController} from '../../../_utils/response.controller';
import {ApiError, DbError} from '../../../_utils/error.controller';
// models
import {IUser, User} from '../../models/users/user';
// configs
import {AuthController, IAuthController} from '../_utils/auth.controller';
import {IApiConfig} from '../../../_configs/api.config';


export namespace IUserController {

  export namespace Sign {

    export interface Body {
      googleToken: string;
    }

    export interface Data {
      user: IUser.Data.Item;
    }

    export type Response = [{
      token: string;
      name: null | string;
    }]
  }

  export namespace SignCreate {

    export interface Body {
      googleToken: string;
    }

    export interface Data {
      userID: string;
    }

    export type Response = [{
      token: string;
      name: null | string;
    }]
  }
}

export class UserController {

  static async sign(req: IApiConfig.Options.ReqModified, res: IApiConfig.Options.ResModified) {
    const _Body: IUserController.Sign.Body = req.body;

    const googleToken = _Body.googleToken;
    if (googleToken === void 0) {
      return ResponseController.fail(res, new ApiError('bad params', [{msg: `not correct data`}]));
    }

    const _googleProfile: null | IAuthController.Data.GoogleProfile = await AuthController.checkSecureOauthGoogle(googleToken);
    if (_googleProfile === null) {
      return ResponseController.fail(res,
        new ApiError('bad params', [{mss: `wrong token`}], 'noCatch')
      );
    }

    const _Data: IUserController.Sign.Data = {
      user: null,
    };

    waterfall([

      // get profile if exist
      (callback: { (err: null | DbError) }) => {
        const _opts: IUser.Get.OptsByEmail = {
          email: _googleProfile.email
        };
        User.getByEmail(_opts, (err: null | DbError, users?: IUser.Data.Item[]) => {
          if (users !== void 0 && users[0] !== void 0) {
            _Data.user = users[0];
          }

          callback(err);
        });
      },

    ], (err: null | DbError) => {
      if (err !== null || _Data.user === null) {
        return ResponseController.restricted(res);
      }

      const _opts: IAuthController.Create.Opts = {
        id: _Data.user._id.toString(),
      };
      const token = AuthController.createToken(_opts);

      const dataResponse: IUserController.Sign.Response = [{
        token: token,
        name: _Data.user.name,
      }];
      ResponseController.success(res, dataResponse);
    });
  }

  static async set(req: IApiConfig.Options.ReqModified, res: IApiConfig.Options.ResModified) {
    const _Body: IUserController.SignCreate.Body = req.body;

    const googleToken = _Body.googleToken;
    if (googleToken === void 0) {
      return ResponseController.fail(res, new ApiError('bad params', [{msg: `not correct data`}]));
    }

    const _googleProfile: null | IAuthController.Data.GoogleProfile = await AuthController.checkSecureOauthGoogle(googleToken);
    if (_googleProfile === null) {
      return ResponseController.fail(res,
        new ApiError('bad params', [{mss: `wrong token`}], 'noCatch')
      );
    }

    const _Data: IUserController.SignCreate.Data = {
      userID: null,
    };

    waterfall([

      // validate user creation
      (callback: { (err: null | true) }) => {

        // todo: to permission tree for add next admins
        const allowedEmails: string[] = [
          'petr.nikolaychuk@gmail.com',
        ];

        return callback(
          allowedEmails.indexOf(_googleProfile.email) === -1
            ? true
            : null
        );
      },

      // create profile if not exist
      (callback: { (err: null | DbError) }) => {
        const _opts: IUser.Create.Opts = {
          email: _googleProfile.email,
          name: _googleProfile.name,
        };
        User.create(_opts, (err: null | DbError, userID?: string) => {
          if (userID !== void 0) {
            _Data.userID = userID[0];
          }

          callback(err);
        });
      },

    ], (err: null | DbError) => {
      if (err !== null) {
        return ResponseController.restricted(res);
      }

      const _opts: IAuthController.Create.Opts = {
        id: _Data.userID,
      };
      const token = AuthController.createToken(_opts);

      const dataResponse: IUserController.SignCreate.Response = [{
        token: token,
        name: _googleProfile.name,
      }];
      ResponseController.success(res, dataResponse);
    });
  }
}
