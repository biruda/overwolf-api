'use strict';
// modules
import {ObjectID} from 'mongodb';
// controllers
import {DBController, IDBController} from '../../controllers/_db';
import {DbError} from '../../../_utils/error.controller';
import {IMongoController} from '../../controllers/_db/mongo.controller';
// configs
import {getModelConfig, IModelConfig} from '../../../_configs/model.config';


export namespace IApp {


  export namespace Data {

    export interface Item extends ItemCreate {
      _id: ObjectID;
    }

    export interface ItemCreate {
      AppName: string;
      Author: ObjectID;
      created_at: Date;
    }
  }

  export namespace Storage {

    export interface Internal {
      configs: Configs;
    }

    interface Configs {
      _model: IModelConfig.Model;
    }
  }

  export namespace Get {

    export interface Opts {
      id: string;
    }

    export type Cb = (err: null | DbError, apps?: IApp.Data.Item[]) => void;
  }

  export namespace Create {

    export interface Opts {
      AppName: string;
      Author: ObjectID;
    }

    export type Cb = (err: null | DbError, appID?: string) => void;
  }
}

const _Internal: IApp.Storage.Internal = {
  configs: {
    _model: getModelConfig().apps.app,
  },
};

export class App {

  static get(opts: IApp.Get.Opts, cb: IApp.Get.Cb): void {
    const configs: IDBController.Options.DriverConfig = {
      driverName: _Internal.configs._model.driverName,
      table: _Internal.configs._model.table,
    };
    const paramsMongo: IMongoController.Params.Params = {
      search: {
        _id: new ObjectID(opts.id),
      },
    };
    const params: IDBController.Params.Params = {
      mongo: paramsMongo,
    };

    DBController.get(configs, params, (err: null | DbError, apps?: IApp.Data.Item[]) => cb(err, apps));
  }

  static getAll(cb: IApp.Get.Cb): void {
    const configs: IDBController.Options.DriverConfig = {
      driverName: _Internal.configs._model.driverName,
      table: _Internal.configs._model.table,
    };
    const params: IDBController.Params.Params = {
      mongo: {},
    };

    DBController.get(configs, params, (err: null | DbError, apps?: IApp.Data.Item[]) => cb(err, apps));
  }

  static create(opts: IApp.Create.Opts, cb: IApp.Create.Cb): void {
    const configs: IDBController.Options.DriverConfig = {
      driverName: _Internal.configs._model.driverName,
      table: _Internal.configs._model.table,
    };
    const _Item: IApp.Data.ItemCreate = {
      AppName: opts.AppName,
      Author: opts.Author,
      created_at: new Date(),
    };
    const paramsData: IDBController.Params.Data = {
      mongo: _Item,
    };

    DBController.set(configs, paramsData, (err: null | DbError, data: [ObjectID]) => {
      cb(err, data !== void 0 ? data[0].toString() : undefined);
    });
  }
}
