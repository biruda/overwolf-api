'use strict';
// modules
import {ObjectID} from 'mongodb';
// controllers
import {DBController, IDBController} from '../../controllers/_db';
import {DbError} from '../../../_utils/error.controller';
import {IMongoController} from '../../controllers/_db/mongo.controller';
// configs
import {getModelConfig, IModelConfig} from '../../../_configs/model.config';


export namespace INote {

  export namespace Data {

    export interface ItemPublic {
      VersionNumber: number;
      ReleaseDate: Date;
      ReleaseNotes: string;
    }

    export interface Item extends ItemCreate {
      _id: ObjectID;
    }

    export interface ItemCreate {
      AppID: ObjectID;
      VersionNumber: number;
      ReleaseDate: Date;
      ReleaseNotes: string;
      Published: boolean;
    }
  }

  export namespace Storage {

    export interface Internal {
      configs: Configs;
    }

    interface Configs {
      _model: IModelConfig.Model;
    }
  }

  export namespace Get {

    export interface Opts {
      AppID: string;
    }

    export interface OptsPublic {
      AppID: string;
      VersionNumber: number;
      offset: number;
      limit: number;
    }

    export type Cb = (err: null | DbError, notes?: INote.Data.Item[]) => void;

    export type CbPublic = (err: null | DbError, notes?: INote.Data.ItemPublic[]) => void;
  }

  export namespace Create {

    export interface Opts {
      AppID: ObjectID;
      VersionNumber: number;
      ReleaseDate: Date;
      ReleaseNotes: string;
      Published: boolean;
    }

    export type Cb = (err: null | DbError, noteID?: string) => void;
  }

  export namespace Update {

    export interface Opts {
      noteID: string;
      ReleaseNotes?: string;
      Published?: boolean;
    }

    export type Cb = (err: null | DbError) => void;
  }
}

const _Internal: INote.Storage.Internal = {
  configs: {
    _model: getModelConfig().release.note,
  },
};

export class Note {

  static get(opts: INote.Get.Opts, cb: INote.Get.Cb): void {
    const configs: IDBController.Options.DriverConfig = {
      driverName: _Internal.configs._model.driverName,
      table: _Internal.configs._model.table,
    };
    const paramsMongo: IMongoController.Params.Params = {
      search: {
        AppID: new ObjectID(opts.AppID),
      },
    };
    const params: IDBController.Params.Params = {
      mongo: paramsMongo,
    };

    DBController.get(configs, params, (err: null | DbError, notes?: INote.Data.Item[]) => cb(err, notes));
  }

  static getPublic(opts: INote.Get.OptsPublic, cb: INote.Get.CbPublic): void {
    const configs: IDBController.Options.DriverConfig = {
      driverName: _Internal.configs._model.driverName,
      table: _Internal.configs._model.table,
    };
    const paramsMongo: IMongoController.Params.Params = {
      search: {
        AppID: new ObjectID(opts.AppID),
        VersionNumber: opts.VersionNumber,
        Published: true,
      },
      skip: opts.offset,
      limit: opts.limit,
      sort: {
        ReleaseDate: -1
      },
      fields: {
        _id: 1,
        VersionNumber: 1,
        ReleaseDate: 1,
        ReleaseNotes: 1,
      },
    };
    const params: IDBController.Params.Params = {
      mongo: paramsMongo,
    };

    DBController.get(configs, params, (err: null | DbError, notes?: INote.Data.Item[]) => {
      if (err !== null) {
        return cb(err);
      }

      const notesPublic: INote.Data.ItemPublic[] = [];
      for (let i = 0, len = notes.length; i < len; i++) {
        const note: INote.Data.Item = notes[i];
        notesPublic.push({
          VersionNumber: note.VersionNumber,
          ReleaseDate: note.ReleaseDate,
          ReleaseNotes: note.ReleaseNotes,
        })
      }

      cb(null, notesPublic)
    });
  }

  static create(opts: INote.Create.Opts, cb: INote.Create.Cb): void {
    const configs: IDBController.Options.DriverConfig = {
      driverName: _Internal.configs._model.driverName,
      table: _Internal.configs._model.table,
    };
    const _Item: INote.Data.ItemCreate = {
      AppID: opts.AppID,
      VersionNumber: opts.VersionNumber,
      ReleaseDate: opts.ReleaseDate,
      ReleaseNotes: opts.ReleaseNotes,
      Published: opts.Published,
    };
    const paramsData: IDBController.Params.Data = {
      mongo: _Item,
    };

    DBController.set(configs, paramsData, (err: null | DbError, data: [ObjectID]) => {
      cb(err, data !== void 0 ? data[0].toString() : undefined);
    });
  }

  static update(opts: INote.Update.Opts, cb: INote.Update.Cb): void {
    const configs: IDBController.Options.DriverConfig = {
      driverName: _Internal.configs._model.driverName,
      table: _Internal.configs._model.table,
    };
    const paramsSearchMongo: IMongoController.Params.Search = {
      _id: new ObjectID(opts.noteID)
    };
    const paramsUpdateMongo: IMongoController.Params.Update = {
      $set: {},
    };

    if (opts.ReleaseNotes !== void 0) {
      paramsUpdateMongo.$set.ReleaseNotes = opts.ReleaseNotes;
    }
    if (opts.Published !== void 0) {
      paramsUpdateMongo.$set.Published = opts.Published;
    }

    const paramsSearch: IDBController.Params.Search = {
      mongo: paramsSearchMongo,
    };
    const paramsUpdate: IDBController.Params.Update = {
      mongo: paramsUpdateMongo,
    };

    DBController.update(configs, paramsSearch, paramsUpdate, (err: null | DbError) => cb(err));
  }
}

