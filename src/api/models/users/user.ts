'use strict';
// modules
import {ObjectID} from 'mongodb';
// controllers
import {DBController, IDBController} from '../../controllers/_db';
import {DbError} from '../../../_utils/error.controller';
import {IMongoController} from '../../controllers/_db/mongo.controller';
// configs
import {getModelConfig, IModelConfig} from '../../../_configs/model.config';


export namespace IUser {

  export namespace Data {

    export interface Item extends ItemCreate {
      _id: ObjectID;
    }

    export interface ItemCreate {
      email: string;
      name: string;
      created_at: Date;
    }
  }

  export namespace Storage {

    export interface Internal {
      configs: Configs;
    }

    interface Configs {
      _model: IModelConfig.Model;
    }
  }

  export namespace Get {

    export interface Opts {
      id: string;
    }

    export interface OptsByEmail {
      email: string;
    }

    export type Cb = (err: null | DbError, users?: IUser.Data.Item[]) => void;
  }

  export namespace Create {

    export interface Opts {
      email: string;
      name: string;
    }

    export type Cb = (err: null | DbError, userID?: string) => void;
  }
}

const _Internal: IUser.Storage.Internal = {
  configs: {
    _model: getModelConfig().users.user,
  },
};

export class User {

  static get(opts: IUser.Get.Opts, cb: IUser.Get.Cb): void {
    const configs: IDBController.Options.DriverConfig = {
      driverName: _Internal.configs._model.driverName,
      table: _Internal.configs._model.table,
    };
    const paramsMongo: IMongoController.Params.Params = {
      search: {
        _id: new ObjectID(opts.id),
      },
    };
    const params: IDBController.Params.Params = {
      mongo: paramsMongo,
    };

    DBController.get(configs, params, (err: null | DbError, users?: IUser.Data.Item[]) => cb(err, users));
  }

  static getByEmail(opts: IUser.Get.OptsByEmail, cb: IUser.Get.Cb): void {
    const configs: IDBController.Options.DriverConfig = {
      driverName: _Internal.configs._model.driverName,
      table: _Internal.configs._model.table,
    };
    const paramsMongo: IMongoController.Params.Params = {
      search: {
        email: opts.email,
      },
    };
    const params: IDBController.Params.Params = {
      mongo: paramsMongo,
    };

    DBController.get(configs, params, (err: null | DbError, users?: IUser.Data.Item[]) => cb(err, users));
  }

  static create(opts: IUser.Create.Opts, cb: IUser.Create.Cb): void {
    const configs: IDBController.Options.DriverConfig = {
      driverName: _Internal.configs._model.driverName,
      table: _Internal.configs._model.table,
    };
    const _Item: IUser.Data.ItemCreate = {
      email: opts.email,
      name: opts.name,
      created_at: new Date(),
    };
    const paramsData: IDBController.Params.Data = {
      mongo: _Item,
    };

    DBController.set(configs, paramsData, (err: null | DbError, data: [ObjectID]) => {
      cb(err, data !== void 0 ? data[0].toString() : undefined);
    });
  }
}
