'use strict';
// modules
import {parse as URLParse} from 'url';
import {parse as QSparse} from 'query-string';
import {ServerResponse} from 'http';
import {gunzip} from 'zlib';
// services
import {HttpError} from '../_utils/error.controller';
// routes
import {getRoutesV1} from './routes';
// configs
import {getApiConfig, IApiConfig} from '../_configs/api.config';

namespace IRouter {

  export interface Response {
    err: boolean;
    msg: string | object;
  }

  export namespace Storage {

    export interface Internal {
      data: Data;
      configs: Configs;
    }

    interface Data {
      ROUTES: IApiConfig.Data.RoutesSource;
    }

    interface Configs {
      apiConfig: IApiConfig.Config;
    }
  }
}

const _Internal: IRouter.Storage.Internal = {
  data: {
    ROUTES: {}
  },
  configs: {
    apiConfig: getApiConfig(),
  },
};
_Internal.data.ROUTES['v1'] = getRoutesV1();


export class Router {

  static router(req, res) {
    req.urlObj = URLParse(req.url);
    req.query = req.urlObj.query !== null ? QSparse(req.urlObj.query) : {};
    res.start = Date.now();
    res.url = req.urlObj.path;

    const _method: string = req.method.toLowerCase();

    // cors
    const origin = req.headers.origin;

    // todo: validate list of allowed origins

    if (origin !== void 0 && origin !== null && origin !== '') {
      res.setHeader('Access-Control-Allow-Origin', origin);
    } else {
      res.setHeader('Access-Control-Allow-Origin', '*');
    }

    // res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PUT, DELETE, HEAD');
    res.setHeader('Access-Control-Allow-Credentials', 'true');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, authorization, Accept, X-Requested-With, remember-me');
    res.setHeader('Cache-Control', 'private, no-cache, no-store, must-revalidate');
    res.setHeader('Expires', '-1');
    res.setHeader('Pragma', 'no-cache');

    if (req.urlObj.pathname === '/favicon.ico') {
      return RouterUtils.sendSuccess(res);
    } else if (req.urlObj.pathname === '/robots.txt') {
      return RouterUtils.sendSuccess(res);
    }

    if (_method === 'options') {
      return RouterUtils.sendSuccess(res);
    } else if (['get', 'delete'].indexOf(_method) !== -1) {
      return RouterUtils.executeRequest(req, res, _method);
    } else if (['post', 'put'].indexOf(_method) !== -1) {
      const body = [];
      req.on('data', (chunk) => body.push(chunk))
        .on('end', () => {
          BodyParser.bodyParser(
            req.headers['content-encoding'] === 'gzib' ? 'gzib' : req.headers['content-type'],
            Buffer.concat(body),
            (bodyObj) => {
              req.body = bodyObj;
              RouterUtils.executeRequest(req, res, _method);
            });
        });
    } else {
      RouterUtils.sendError(new HttpError('wrong method', [{
        err: 'not implemented',
        method: _method,
        url: req.url
      }]), res);
    }
  }
}

class RouterUtils {

  static executeRequest(req, res: ServerResponse, _method: string) {
    const method: 'post' | 'get' | 'remove' | string = _method === 'delete' ? 'remove' : _method;
    const _path: string = req.urlObj.pathname;
    const __regVer = /[\/v\d]*\//g;
    const __regStart = /^[\/v\d]*\//g;

    // get request _apiOLD version
    let _v = _path.match(__regVer);
    if (_v === null || __regStart.test(_path) === false) {
      return RouterUtils.sendError(new HttpError('wrong param', [{err: 'Not _api', _path}]), res);
    }
    const _ver = _v[0].substring(1, _v[0].length - 1);
    const _curRoutes: IApiConfig.Data.Routes = _Internal.data.ROUTES[_ver];
    if (_curRoutes === void 0) {
      return RouterUtils.sendError(new HttpError('wrong param', [{err: 'Not _api', _path, _ver}]), res);
    }

    // get path without _apiOLD' version
    const _mod = _path.substring(_v.length + 1);
    let route = null;
    try {
      route = _curRoutes[method][_mod];
    } catch (e) {
    }
    if (route === null || route === void 0) {
      return RouterUtils.sendError(new HttpError('wrong param', [{err: 'Not found', _path, _ver, method, _mod}]), res);
    }

    route(req, res);
  }

  static sendError(err: HttpError, res: ServerResponse) {
    const _data: IRouter.Response = {err: true, msg: err.message};

    res.setHeader('Content-Type', 'application/json');
    res.writeHead(400);
    res.end(JSON.stringify(_data));
  }

  static sendSuccess(res: ServerResponse) {
    res.writeHead(200);
    res.end();
  }
}

class BodyParser {

  static bodyParser(contentType: string, body: Buffer, cb: { (data: object) }): void {
    if (contentType === 'application/x-www-form-urlencoded') {
      cb(BodyParserUtils.parseString(body.toString('utf8')));
    } else if (contentType === 'application/json') {
      cb(BodyParserUtils.parseJson(body.toString('utf8')));
    } else if (contentType === 'gzib') {
      BodyParserUtils.parseGzip(body, (data: object) => cb(data));
    } else if (contentType.indexOf('multipart/form-data') !== -1) {
      cb(BodyParserUtils.parseJson(body.toString('utf8')));
    }
  }
}

class BodyParserUtils {

  static parseString(str: string) {
    if (!str) {
      return {};
    }

    const b = {};
    str.split('&').forEach((pair) => {
      try {
        const v = pair.split('=');
        b[v[0]] = JSON.parse(v[1]);
      } catch (e) {
      }
    });

    return b;
  }

  static parseJson(jsonStr: string): object {
    let b: object = {};
    try {
      b = JSON.parse(jsonStr);
    } catch (e) {
      b['string'] = jsonStr;
    }
    return b;
  }

  static parseGzip(content: Buffer, cb: { (data: object) }) {
    gunzip(content, (err, decodeContent) => {
      if (err !== null) {
        return cb({});
      }

      cb(decodeContent.toJSON());
    });
  }
}
