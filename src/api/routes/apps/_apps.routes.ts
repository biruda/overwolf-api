'use strict';
import {AppRoutes} from './app.routes';
import {IApiConfig, prepareRoutesGroup} from '../../../_configs/api.config';

export const AppsGroupRoutes: IApiConfig.Data.Routes = prepareRoutesGroup({
  AppRoutes: AppRoutes,
});
