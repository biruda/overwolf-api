'use strict';
import {getApiConfig, IApiConfig} from '../../../_configs/api.config';
import {AppController} from '../../controllers/apps/app.controller';

const _routes: IApiConfig.Data.Routes = {
  get: {},
  put: {},
  post: {},
  remove: {},
};

const _apps = getApiConfig().url.apps;
_routes.get[_apps.app] = (req, res) => AppController.get(req, res);
_routes.put[_apps.app] = (req, res) => AppController.set(req, res);

export const AppRoutes: IApiConfig.Data.Routes = _routes;
