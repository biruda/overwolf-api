'use strict';
// routes
import {AppsGroupRoutes} from './apps/_apps.routes';
import {ReleaseGroupRoutes} from './release/_release.routes';
import {UsersGroupRoutes} from './users/_users.routes';
// configs
import {IApiConfig, prepareRoutesGroup} from '../../_configs/api.config';

export function getRoutesV1(): IApiConfig.Data.Routes {
  return prepareRoutesGroup({
    AppsGroupRoutes: AppsGroupRoutes,
    ReleaseGroupRoutes: ReleaseGroupRoutes,
    UsersGroupRoutes: UsersGroupRoutes,
  });
}
