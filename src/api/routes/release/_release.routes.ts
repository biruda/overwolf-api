'use strict';
import {NoteRoutes} from './note.routes';
import {IApiConfig, prepareRoutesGroup} from '../../../_configs/api.config';

export const ReleaseGroupRoutes: IApiConfig.Data.Routes = prepareRoutesGroup({
  NoteRoutes: NoteRoutes,
});
