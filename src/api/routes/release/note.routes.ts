'use strict';
import {getApiConfig, IApiConfig} from '../../../_configs/api.config';
import {NoteController} from '../../controllers/release/note.controller';

const _routes: IApiConfig.Data.Routes = {
  get: {},
  put: {},
  post: {},
  remove: {},
};

const _release = getApiConfig().url.release;
_routes.get[_release.note] = (req, res) => NoteController.get(req, res);
_routes.get[_release.notePublic] = (req, res) => NoteController.getPublic(req, res);
_routes.put[_release.note] = (req, res) => NoteController.set(req, res);
_routes.post[_release.note] = (req, res) => NoteController.update(req, res);

export const NoteRoutes: IApiConfig.Data.Routes = _routes;
