'use strict';
import {UserRoutes} from './user.routes';
import {IApiConfig, prepareRoutesGroup} from '../../../_configs/api.config';

export const UsersGroupRoutes: IApiConfig.Data.Routes = prepareRoutesGroup({
  UserRoutes: UserRoutes,
});
