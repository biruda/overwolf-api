'use strict';
import {getApiConfig, IApiConfig} from '../../../_configs/api.config';
import {UserController} from '../../controllers/users/user.controller';

const _routes: IApiConfig.Data.Routes = {
  get: {},
  put: {},
  post: {},
  remove: {},
};

const _users = getApiConfig().url.users;
_routes.post[_users.authGoogle] = (req, res) => UserController.sign(req, res);
_routes.put[_users.authGoogle] = (req, res) => UserController.set(req, res);

export const UserRoutes: IApiConfig.Data.Routes = _routes;
