'use strict';
// modules
import {parallel} from 'async';
import {Router} from './router';
import {createServer as HTTP_createServer} from 'http';
import {Server} from 'net';
// models
import {DBController as DB_v1} from './controllers/_db/index';
// controllers
import {DbError, ServerError} from '../_utils/error.controller';
// configs
import {getServerConfig, IServerConfig} from '../_configs/server.config';
import {getApiConfig, IApiConfig} from '../_configs/api.config';


export namespace IAPI {

  export namespace Storage {

    export interface Internal {
      configs: Configs;
    }

    interface Configs {
      _serverConfig: IServerConfig.Config;
      _apiConfig: IApiConfig.Config;
    }
  }
}

const _Internal: IAPI.Storage.Internal = {
  configs: {
    _serverConfig: getServerConfig(),
    _apiConfig: getApiConfig(),
  },
};

export class API {

  static start(cb: { (err: null | ServerError, data?: string) }) {
    Utils.before((err: null | DbError) => {
      if (err !== null) {
        return cb(new ServerError('server not started', [{err: 'start', message: err.message}]));
      }

      Utils.init((err: null) => {
        if (err !== null) {
          return cb(new ServerError('server not started', [{err: 'start', message: err}]));
        }
      });
    });
  }
}

class Utils {

  static before(cb: { (err: null | DbError) }) {
    parallel([

      (callback: { (err: null | DbError) }) => {
        DB_v1.connectAll((err: null | DbError) => callback(err));
      },

    ], (err: null | DbError) => cb(err));
  }

  static init(cb: { (err: null, port: number, server: Server) }) {
    if (process.env['NODE_ENV'] === 'development') {
      const server: Server = HTTP_createServer((request, response) => Router.router(request, response))
        .listen(_Internal.configs._serverConfig.api.serverPortDev, () => cb(null, _Internal.configs._serverConfig.api.serverPortDev, server));
    } else {
      // const options = {
      //   key: readFileSync('*.ai.key'),
      //   cert: readFileSync('*.ai.crt'),
      //   requestCert: false,
      //   rejectUnauthorized: true
      // };
      // const server: Server = HTTPS_createServer(options, (request: IncomingMessage, response: ServerResponse) => {
      //   _Internal.controllers.router.router(request, response);
      // }).listen(_Internal.configs._serverConfig.api.serverPortProd, (err) => cb(err, 443, server));
      const server: Server = HTTP_createServer((request, response) => Router.router(request, response))
        .listen(_Internal.configs._serverConfig.api.serverPortProd, () => cb(null, _Internal.configs._serverConfig.api.serverPortProd, server));
    }
  }
}
