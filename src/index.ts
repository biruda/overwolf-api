'use strict';
// modules
import {waterfall} from 'async';
import {API} from './api/server';
// controllers
import {ErrorBase} from './_utils/error.controller';

// main flow
waterfall([

  // start API
  (callback: { (err: null | ErrorBase) }) => {
    API.start((err: null | ErrorBase, data?: string) => callback(err));
  },

], (err: null | ErrorBase) => {
  console.log('Project initialisation', {err});

  process.on('SIGINT', () => {
    console.log('Project stopped - SIGINT');

    // actions to safety stop
    waterfall([], () => {
      console.log('safety stop');
      process.exit();
    });
  });
});
